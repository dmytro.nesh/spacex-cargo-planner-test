import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Link, useParams } from 'react-router-dom';
import classNames from 'classnames';

import { Input } from '../Input';
import { logo, searchIcon } from '../../assets/images';

import styles from './Header.module.css';

const Header = ({ toggleMenu, handleSearch }) => {
  const params = useParams();
  const [menuIsOpen, setMenuIsOpen] = useState(false);
  const [searchQuery, setSearchQuery] = useState('');

  const onMenuToggle = () => {
    setMenuIsOpen(!menuIsOpen);
    toggleMenu();
  };

  const onSearch = (val) => {
    setSearchQuery(val);
    handleSearch(val);
  };

  const shipmentId = params.shipmentId;

  useEffect(() => {
    setMenuIsOpen(false)
  }, [shipmentId]);

  return (
    <header className={styles.header}>
      <div className={styles.logo}>
        <Link to='/'>
          <img src={logo} alt="" />
        </Link>
      </div>
      <div className={styles.burgerMenu}>
        <div
          role='button'
          className={classNames(styles.menuIcon, {[styles.isActive]: menuIsOpen})}
          onClick={onMenuToggle}
        >
          <span />
          <span />
          <span />
        </div>
      </div>

      <div className={styles.search}>
        <Input
          startIcon={<img src={searchIcon} alt='search'/>}
          placeholder='Search'
          value={searchQuery}
          onChange={onSearch}
        />
      </div>
    </header>
  );
};

Header.propTypes = {
  toggleMenu: PropTypes.func.isRequired,
  handleSearch: PropTypes.func.isRequired,
};

export default Header;