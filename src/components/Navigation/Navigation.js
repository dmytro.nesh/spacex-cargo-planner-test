import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classNames from 'classnames';

import styles from './Navigation.module.css';

const Navigation = ({ items, activeItem, isVisible }) => {
  return (
    <nav className={classNames(styles.nav, {[styles.isVisible]: isVisible})}>
      <h4>Shipment list</h4>
      {items && (
        <div className={styles.links}>
          {items.map(({id, name}) => (
            <Link
              key={id}
              to={`/${id}`}
              className={classNames(styles.link, {[styles.isActive]: activeItem === id})}
            >
              {name}
            </Link>
          ))}
        </div>
      )}
    </nav>
  );
};

Navigation.defaultProps = {
  activeItem: '',
  isVisible: false
};

Navigation.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string
  })).isRequired,
  activeItem: PropTypes.string,
  isVisible: PropTypes.bool
};

export default Navigation;