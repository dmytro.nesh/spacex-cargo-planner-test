import React, { useEffect, useMemo, useState } from 'react';
import { useParams } from 'react-router-dom';

import { Layout, Input } from '../../components';
import { getShipmentsList } from '../../services/api';

import styles from './Homepage.module.css';

const unitsLimit = 10;

const calculateCargoCapacity = (items) => {
  const num = items.split(',').reduce((num, value) => {
    const valueNum = +value;
    return isNaN(valueNum) ? num : num + valueNum;
  }, 0);

  return Math.ceil(num / unitsLimit);
};

const Homepage = ({}) => {
  const params = useParams();

  const [shipmentsState, setShipmentsState] = useState([]);
  const [items, setItems] = useState('');
  const [error, setError] = useState('');

  const getShipmentsData = async () => {
    const data = await getShipmentsList();
    setShipmentsState(data);
  };

  useEffect(() => {
    // noinspection JSIgnoredPromiseFromCall
    getShipmentsData();
  }, []);

  const currentShipmentId = params.shipmentId;
  const currentShipment = shipmentsState.find(s => s.id === currentShipmentId);

  useEffect(() => {
    if (currentShipment) setItems(currentShipment.boxes);
  }, [currentShipment]);

  const requiredCargoCapacity = useMemo(() => calculateCargoCapacity(items), [items]);

  const noShipmentContent = () => (
    <div className={styles.noShipment}>
      <p>Please, select a shipment from the Shipments list</p>
    </div>
  );

  const handleShipmentChange = (val) => {
    setItems(val);

    const nums = val.split(',');
    const isInvalid = nums.some(v => isNaN(+v));
    const outOfRange = nums.some(v => {
      const valueNum = +v;
      return valueNum < 0 || valueNum > 10;
    });

    if (isInvalid) {
      return setError('Please, enter numbers only, separated by a comma');
    }
    if (outOfRange) {
      return setError(`Cargo box is contain up to ${unitsLimit} units`);
    }

    setError('');
  };

  return (
    <Layout
      shipmentsData={shipmentsState}
      shipmentId={currentShipmentId}
    >
      <div className={styles.cargoForm}>
        {currentShipment ? (
          <>
            <div className={styles.shipment}>
              <div className={styles.shipmentName}>{currentShipment.name}</div>
              <div>{currentShipment.email}</div>
              <div className={styles.shipmentBoxes}>
                <label className={styles.formLabel}>Cargo boxes</label>
                <Input
                  value={items}
                  onChange={handleShipmentChange}
                />
              </div>
              {!!error && <div className={styles.error}>{error}</div>}
            </div>
            {requiredCargoCapacity && !error && (
              <div className={styles.shipmentResult}>
                <div>Number of required cargo bays</div>
                <div className={styles.reqNum}>{requiredCargoCapacity}</div>
              </div>
            )}
          </>
        ) : noShipmentContent()}
      </div>
    </Layout>
  );
};

export default Homepage;