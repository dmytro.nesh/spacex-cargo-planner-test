import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { Homepage } from './pages';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<Homepage />} />
        <Route path='/:shipmentId' element={<Homepage />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
