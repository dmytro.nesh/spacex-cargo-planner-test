import { useLayoutEffect } from 'react';

export default (state) => {
  useLayoutEffect(() => {
    const originalStyleBody = window.getComputedStyle(document.body).overflow;
    const originalStyleRoot = window.getComputedStyle(document.documentElement).overflow;

    if (state) {
      document.body.style.overflow = 'hidden';
      document.documentElement.style.overflow = 'hidden';
    } else {
      document.body.style.overflow = originalStyleBody;
      document.documentElement.style.overflow = originalStyleRoot;
    }

    return () => {
      document.body.style.overflow = originalStyleBody;
      document.documentElement.style.overflow = originalStyleRoot;
    }
  }, [state]);
}