import React from 'react';
import PropTypes from 'prop-types';

import styles from './Input.module.css';

const Input = ({ startIcon, type, placeholder, value, onChange }) => {
  return (
    <div className={styles.inputWrapper}>
      {!!startIcon && startIcon}
      <input
        type={type}
        placeholder={placeholder}
        value={value}
        className={styles.input}
        onChange={(e) => onChange(e.target.value)}
      />
    </div>
  );
};

Input.defaultProps = {
  type: 'text',
  max: null,
  placeholder: ''
};

Input.propTypes = {
  startIcon: PropTypes.node,
  type: PropTypes.string,
  max: PropTypes.number,
  placeholder: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func
};

export default Input;