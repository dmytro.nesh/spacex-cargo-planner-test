export const getShipmentsList = async () => {
  try {
    const response = await fetch(`${window.location.origin}/shipments.json`);
    return await response.json();
  } catch (e) {
    console.error(e);
  }
};