import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import { Header } from '../Header';
import { Navigation } from '../Navigation';
import scrollLock from '../../helpers/scrollLock';

import styles from './Layout.module.css';

const Layout = ({ shipmentsData, shipmentId, children }) => {
  const [menuIsOpen, setMenuIsOpen] = useState(false);
  const [shipmentsItems, setShipmentsItems] = useState([]);

  useEffect(() => {
    setShipmentsItems(shipmentsData);
  }, [shipmentsData]);

  useEffect(() => {
    setMenuIsOpen(false);
  }, [shipmentId]);

  const handleMenuToggle = () => {
    setMenuIsOpen(!menuIsOpen);
  };

  scrollLock(menuIsOpen);

  const handleSearch = (val) => {
    const filteredItems = shipmentsData.filter(i => i.name.toLocaleLowerCase().includes(val.toLocaleLowerCase()));
    setShipmentsItems(filteredItems);
  };

  return (
    <div className={styles.page}>
      <Header toggleMenu={handleMenuToggle} handleSearch={handleSearch} />
      <Navigation items={shipmentsItems} activeItem={shipmentId} isVisible={menuIsOpen} />
      {children && (
        <main className={styles.content}>
          {children}
        </main>
      )}
    </div>
  );
};

Layout.defaultProps = {
  shipmentId: ''
};

Layout.propTypes = {
  shipmentsData: PropTypes.arrayOf(PropTypes.object).isRequired,
  shipmentId: PropTypes.string,
  children: PropTypes.node
};

export default Layout;